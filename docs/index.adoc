= Subconvert Manual
:author: Michał Góral
:revnumber: @VERSION@
:toc: left
:toclevels: 2
:doctype: book
:icons: font
:sectnums:
:docinfo1:
:experimental:
:imagesdir: img
:snipdir: snippets

Subconvert is Python application which is capable to do convertions between
various popular subtitle formats. It is also smart enough to automatically
detect files format, encoding and even movie FPS (if there is any movie given).

= User Manual

include::manual/installation.adoc[]

include::manual/usage.adoc[]

include::manual/program-options.adoc[]

include::manual/gui.adoc[]

include::manual/pfile.adoc[]

= Developer Manual

include::dev-related/translating.adoc[]

[appendix]
include::faq.adoc[]

[appendix]
include::manual/available-formats.adoc[]

[appendix]
include::manual/encodings.adoc[]

[appendix]
include::manual/glossary.adoc[]

// vim: set tw=80 colorcolumn=81 ft=asciidoc ft=asciidoc :
