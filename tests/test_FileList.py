# Copyright (C) 2017 Michal Goral.
#
# This file is part of Subconvert
#
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest

from PyQt5.QtCore import Qt, QPoint, QItemSelectionModel

from subconvert.gui.SubtitleCommands import NewSubtitles, RemoveFile
from subconvert.gui.SubtitleTabs import FileList

@pytest.fixture
def filelist(qtbot, subdata):
    w = FileList('subtitlelist', subdata)
    qtbot.addWidget(w)
    w.show()
    return w


def _select(rows, filelist):
    fl = filelist._fileList
    sm = fl.selectionModel()
    m = fl.model()

    # clear previous selection and select all requested rows
    mode = QItemSelectionModel.ClearAndSelect
    for row in rows:
        idx = m.index(row, 0)
        sm.select(idx, mode | QItemSelectionModel.Rows)
        mode = QItemSelectionModel.Select

def _add_subs(subs, subdata):
    for sub in subs:
        subdata.execute(NewSubtitles(sub))


def test_file_display(filelist, sublist, subdata):
    subs = sublist('subs')
    _add_subs(subs, subdata)

    assert filelist.filePaths == subs
    for sub in subs:
        subdata.execute(RemoveFile(sub))
        assert sub not in filelist.filePaths
    assert len(filelist.filePaths) == 0


def test_selection(filelist, sublist, subdata):
    '''Test if file list correctly reports selected items. This test is
    prerequisite for the other tests which select items.'''
    subs = sublist('subs')
    _add_subs(subs, subdata)
    _select(range(len(subs)), filelist)
    selected_paths = [item.text(0) for item in filelist.selectedItems]
    assert selected_paths == subs

    _select([0, 1, len(subs) - 1], filelist)
    selected_paths = [item.text(0) for item in filelist.selectedItems]
    assert selected_paths == [subs[0], subs[1], subs[-1]]


def test_change_properties_then_undo_and_redo(filelist, sublist, subdata, subparser, genvid):
    '''Checks if only selected files properties are changed'''
    subs = sublist('subs')
    _add_subs(subs, subdata)
    _select([0, 2], filelist)

    sub1_data_copy = subdata.data(subs[1])

    for fmt in subparser.formats:
        filelist.changeSelectedFilesFormat(fmt)
        assert subdata.data(subs[0]).outputFormat == fmt
        assert subdata.data(subs[2]).outputFormat == fmt
        assert subdata.data(subs[1]).outputFormat == sub1_data_copy.outputFormat

    video = genvid(fps=30, time=1)
    filelist.changeSelectedFilesVideoPath(video)
    assert subdata.data(subs[0]).videoPath == video
    assert subdata.data(subs[2]).videoPath == video
    assert subdata.data(subs[1]).videoPath is None

    filelist.detectSelectedFilesFps()
    assert subdata.data(subs[0]).fps == 30
    assert subdata.data(subs[2]).fps == 30
    assert subdata.data(subs[1]).fps == sub1_data_copy.fps

    filelist.changeSelectedFilesInputEncoding('utf-8')
    assert subdata.data(subs[0]).inputEncoding == 'utf-8'
    assert subdata.data(subs[2]).inputEncoding == 'utf-8'
    assert subdata.data(subs[1]).inputEncoding == sub1_data_copy.inputEncoding

    filelist.changeSelectedFilesInputEncoding('ascii')
    assert subdata.data(subs[0]).inputEncoding == 'ascii'
    assert subdata.data(subs[2]).inputEncoding == 'ascii'
    assert subdata.data(subs[1]).inputEncoding == sub1_data_copy.inputEncoding

    filelist.changeSelectedFilesOutputEncoding('utf-8')
    assert subdata.data(subs[0]).outputEncoding == 'utf-8'
    assert subdata.data(subs[2]).outputEncoding == 'utf-8'
    assert subdata.data(subs[1]).outputEncoding == sub1_data_copy.outputEncoding

    filelist.changeSelectedFilesOutputEncoding('ascii')
    assert subdata.data(subs[0]).outputEncoding == 'ascii'
    assert subdata.data(subs[2]).outputEncoding == 'ascii'
    assert subdata.data(subs[1]).outputEncoding == sub1_data_copy.outputEncoding

    filelist.undoSelectedFiles()
    assert subdata.data(subs[0]).outputEncoding == 'utf-8'
    assert subdata.data(subs[2]).outputEncoding == 'utf-8'
    assert subdata.data(subs[1]).outputEncoding == sub1_data_copy.outputEncoding

    filelist.redoSelectedFiles()
    assert subdata.data(subs[0]).inputEncoding == 'ascii'
    assert subdata.data(subs[2]).inputEncoding == 'ascii'
    assert subdata.data(subs[1]).inputEncoding == sub1_data_copy.inputEncoding
