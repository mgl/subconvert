# Copyright (C) 2017 Michal Goral.
#
# This file is part of Subconvert
#
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import pytest
import locale
from collections import namedtuple

from unittest.mock import Mock, call, ANY

from PyQt5.QtWidgets import QStackedWidget

from subconvert.gui.MainWindow import MainWindow
from subconvert.gui.VideoWidget import VideoWidget
from subconvert.gui.ToolBox import ToolBox
from subconvert.gui.SubtitleWindow import SubTabWidget
from subconvert.gui.SubtitleTabs import SubtitleEditor


def _verify_subtitles_opened(subs, subdata):
    for s in subs:
        assert subdata.fileExists(s)


@pytest.fixture
def window(qtbot, subparser):
    locale.setlocale(locale.LC_ALL, 'C')
    w = MainWindow(subparser)
    qtbot.addWidget(w)
    w.show()
    return w


@pytest.fixture
def dialogmock():
    instancemock = Mock()
    classmock = Mock(return_value=instancemock)
    _T = namedtuple('DialogMocks', ('cls', 'instance'))
    return _T(classmock, instancemock)


def test_main_window_widgets(window):
    assert window.findChild(VideoWidget, 'video_player') is not None
    assert window.findChild(ToolBox, 'sidebar') is not None
    assert window.findChild(QStackedWidget, 'pages') is not None
    assert window.findChild(SubTabWidget, 'main_display') is not None


def test_widget_properties(window):
    d = window.findChild(SubTabWidget, 'main_display')

    assert d.currentPage() is d.fileList  # default page is file list
    assert d.fileList.canClose() is False
    assert d.fileList.isStatic is True


def test_open_files(window, sublist):
    paths = sublist('subs')
    assert len(paths) > 1

    window.openFiles(paths, None)
    d = window.findChild(SubTabWidget, 'main_display')
    assert d.fileList.filePaths == paths

    for p in paths:
        d.openTab(p)
        page = d.currentPage()
        assert isinstance(page, SubtitleEditor)
        assert page.filePath == p
        assert len(page.subtitles) > 0


def test_open_dialog(window, dialogmock, monkeypatch, sublist):
    subs = sublist('subs')
    mockconf = {
        'exec.return_value': True,
        'selectedFiles.return_value': subs,
        'getEncoding.return_value': None,
    }
    dialogmock.instance.configure_mock(**mockconf)
    monkeypatch.setattr('subconvert.gui.MainWindow.FileDialog', dialogmock.cls)

    window.openFile()
    assert call.exec() in dialogmock.instance.mock_calls
    assert len(subs) == window.subdata.count()
    _verify_subtitles_opened(subs, window.subdata)


def test_open_dialog_exec_fail(window, dialogmock, monkeypatch):
    mockconf = {'exec.return_value': False}
    dialogmock.instance.configure_mock(**mockconf)
    monkeypatch.setattr('subconvert.gui.MainWindow.FileDialog', dialogmock.cls)

    window.openFile()
    assert call.exec() in dialogmock.instance.mock_calls
    assert window.subdata.count() == 0


def test_open_error_dialog(window, dialogmock, sublist, monkeypatch):
    bad = sublist('subs_incorrect')
    good = sublist('subs')

    mockconf = {'exec.return_value': True}
    dialogmock.instance.configure_mock(**mockconf)
    monkeypatch.setattr('subconvert.gui.MainWindow.CannotOpenFilesMsg', dialogmock.cls)

    window.openFiles(bad + good, None)
    assert call.exec() in dialogmock.instance.mock_calls
    assert call.setFileList(ANY) in dialogmock.instance.mock_calls
    assert len(good) == window.subdata.count()
    _verify_subtitles_opened(good, window.subdata)
