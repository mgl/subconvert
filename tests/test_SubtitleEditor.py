# Copyright (C) 2017 Michal Goral.
# 
# This file is part of Subconvert
# 
# Subconvert is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Subconvert is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Subconvert. If not, see <http://www.gnu.org/licenses/>.

import copy
import pytest

from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtWidgets import QMessageBox

from subconvert.gui.SubtitleTabs import SubtitleEditor

from subconvert.parsing.FrameTime import FrameTime
from subconvert.gui.SubtitleCommands import NewSubtitles

@pytest.fixture
def subeditor(qtbot, subparser, subdata, gensubs):
    sub = gensubs(10)
    subdata.execute(NewSubtitles(sub))
    w = SubtitleEditor(sub, subdata)
    qtbot.addWidget(w)
    w.show()
    return w


def _edit_cell(qtbot, table_view, row, column, intext):
    # https://stackoverflow.com/a/12604740/1088577
    y = table_view.rowViewportPosition(row) + 2
    x = table_view.columnViewportPosition(column) + 3
    vp = table_view.viewport()

    qtbot.mouseClick(vp, Qt.LeftButton, pos=QPoint(x, y))
    qtbot.mouseDClick(vp, Qt.LeftButton, pos=QPoint(x, y))

    focused = vp.focusWidget()
    qtbot.keyClick(focused, Qt.Key_A, Qt.ControlModifier)
    qtbot.keyClick(focused, Qt.Key_Delete)
    qtbot.keyClicks(focused, intext)
    qtbot.keyClick(focused, Qt.Key_Enter)

    # It seems that without waiting some interval, qt fails to properly update a
    # cell. I hate that. :/
    qtbot.wait(1)


def test_select_subtitle(subeditor):
    for i, sub in enumerate(subeditor.subtitles):
        subeditor.selectRow(i)
        assert subeditor.selectedSubtitles() == [sub]


def test_add_subtitle(subeditor):
    orig_subs = subeditor.subtitles.clone()

    subeditor.addNewSubtitle()
    assert len(subeditor.subtitles) == len(orig_subs) + 1
    for i, sub in enumerate(orig_subs):
        assert orig_subs[i] == subeditor.subtitles[i]
    assert subeditor.subtitles[-1].start > subeditor.subtitles[-2].start
    assert subeditor.subtitles[-1].end > subeditor.subtitles[-2].end
    assert subeditor.subtitles[-1].text == ''


def test_insert_subtitle(subeditor):
    orig_subs = subeditor.subtitles.clone()

    subeditor.selectRow(0)
    subeditor.insertNewSubtitle()

    assert len(subeditor.subtitles) == len(orig_subs) + 1
    assert subeditor.subtitles[0].start <= subeditor.subtitles[1].start
    assert subeditor.subtitles[0].end <= subeditor.subtitles[1].end
    assert subeditor.subtitles[0].text == ''

    subeditor.selectRow(len(subeditor.subtitles) - 1)
    subeditor.insertNewSubtitle()

    assert len(subeditor.subtitles) == len(orig_subs) + 2
    assert subeditor.subtitles[-2].start > subeditor.subtitles[-3].start
    assert subeditor.subtitles[-2].end > subeditor.subtitles[-3].end
    assert subeditor.subtitles[-2].start < subeditor.subtitles[-1].start
    assert subeditor.subtitles[-2].end < subeditor.subtitles[-1].end
    assert subeditor.subtitles[-2].text == ''


def test_delete_subtitle(subeditor, qtbot):
    orig_subs = subeditor.subtitles.clone()

    subeditor.selectRow(0)
    subeditor.removeSelectedSubtitles()
    assert len(subeditor.subtitles) == len(orig_subs) - 1
    assert subeditor.subtitles[0] == orig_subs[1]


def test_edit_subtitle(subeditor, qtbot):
    orig_subs = subeditor.subtitles.clone()

    _edit_cell(qtbot, subeditor._subList, 0, 0, 'foo')
    _edit_cell(qtbot, subeditor._subList, 0, 1, 'foo')
    assert subeditor.subtitles[0].start == orig_subs[0].start
    assert subeditor.subtitles[0].end == orig_subs[0].end

    newtime = subeditor.subtitles[0].start + FrameTime(10000, subeditor.subtitles.fps)
    _edit_cell(qtbot, subeditor._subList, 0, 0, newtime.toStr())
    _edit_cell(qtbot, subeditor._subList, 0, 1, newtime.toStr())
    assert subeditor.subtitles[0].start == newtime
    assert subeditor.subtitles[0].end == newtime

    _edit_cell(qtbot, subeditor._subList, 0, 2, subeditor.subtitles[0].text + 'foo bar baz')
    assert subeditor.subtitles[0].text == orig_subs[0].text + 'foo bar baz'


def test_search(subeditor, qtbot):
    def _search_for(text):
        subeditor.showSearch()
        qtbot.keyClick(subeditor._searchBar._editor, Qt.Key_Delete)
        qtbot.keyClicks(subeditor._searchBar._editor, text)
        qtbot.keyClick(subeditor._searchBar._editor, Qt.Key_Enter)

    for i, sub in enumerate(subeditor.subtitles):
        _search_for(sub.text)
        assert subeditor.selectedRows() == [i]

    for i, sub in enumerate(subeditor.subtitles):
        _search_for(sub.text.lower())
        assert subeditor.selectedRows() == [i]

    errstyle = 'background-color: #CD5555'
    _search_for('foo bar baz delta delta <<<')
    assert subeditor._searchBar._editor.styleSheet() == errstyle

    _search_for('')
    assert subeditor._searchBar._editor.styleSheet() == ''
        

def test_change_properties_and_undo_and_redo(subeditor, subparser, qtbot):
    """Go through things which can be changed and then undo and redo them"""
    orig_data = copy.deepcopy(subeditor.data)

    subeditor.selectRow(0)

    subeditor.changeFps(orig_data.fps + 10)
    assert subeditor.data.fps == orig_data.fps + 10

    subeditor.changeInputEncoding('utf-8')
    assert subeditor.data.inputEncoding == 'utf-8'
    subeditor.changeInputEncoding('ascii')
    assert subeditor.data.inputEncoding == 'ascii'

    subeditor.changeOutputEncoding('utf-8')
    assert subeditor.data.outputEncoding == 'utf-8'
    subeditor.changeOutputEncoding('ascii')
    assert subeditor.data.outputEncoding == 'ascii'

    assert len(subparser.formats) > 2
    for fmt in subparser.formats:
        subeditor.changeSubFormat(fmt)
        assert subeditor.outputFormat == fmt

    last_data = copy.deepcopy(subeditor.data)

    # revert all changes
    while subeditor.history.canUndo():
        subeditor.history.undo()

    assert subeditor.data.fps == orig_data.fps
    assert subeditor.data.inputEncoding == orig_data.inputEncoding
    assert subeditor.data.outputEncoding == orig_data.outputEncoding
    assert subeditor.data.outputFormat == orig_data.outputFormat

    # redo all changes
    while subeditor.history.canRedo():
        subeditor.history.redo()

    assert subeditor.data.fps == last_data.fps
    assert subeditor.data.inputEncoding == last_data.inputEncoding
    assert subeditor.data.outputEncoding == last_data.outputEncoding
    assert subeditor.data.outputFormat == last_data.outputFormat


def test_video_linking(subeditor, genvid):
    """Check successful and unsuccessful linking of subtitles with some
    arbitrary video. Linked video is then used to detect FPS."""
    assert subeditor.data.videoPath is None
    assert subeditor.data.fps == 23.976

    subeditor.detectFps()
    assert subeditor.data.videoPath is None
    assert subeditor.data.fps == 23.976

    video = genvid(fps=30, time=1)
    subeditor.changeVideoPath(video)
    subeditor.detectFps()

    assert subeditor.data.videoPath == video
    assert subeditor.data.fps == 30
